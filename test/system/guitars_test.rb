require "application_system_test_case"

class GuitarsTest < ApplicationSystemTestCase
  setup do
    @guitar = guitars(:one)
  end

  test "visiting the index" do
    visit guitars_url
    assert_selector "h1", text: "Guitars"
  end

  test "should create guitar" do
    visit guitars_url
    click_on "New guitar"

    fill_in "Body type", with: @guitar.body_type
    fill_in "Brand", with: @guitar.brand
    fill_in "Bridge type", with: @guitar.bridge_type
    fill_in "Case included", with: @guitar.case_included
    fill_in "Comments", with: @guitar.comments
    fill_in "Condition", with: @guitar.condition
    fill_in "Link", with: @guitar.link
    fill_in "Model", with: @guitar.model
    fill_in "Nut width", with: @guitar.nut_width
    fill_in "Offers", with: @guitar.offers
    fill_in "Price", with: @guitar.price
    fill_in "Wood type", with: @guitar.wood_type
    click_on "Create Guitar"

    assert_text "Guitar was successfully created"
    click_on "Back"
  end

  test "should update Guitar" do
    visit guitar_url(@guitar)
    click_on "Edit this guitar", match: :first

    fill_in "Body type", with: @guitar.body_type
    fill_in "Brand", with: @guitar.brand
    fill_in "Bridge type", with: @guitar.bridge_type
    fill_in "Case included", with: @guitar.case_included
    fill_in "Comments", with: @guitar.comments
    fill_in "Condition", with: @guitar.condition
    fill_in "Link", with: @guitar.link
    fill_in "Model", with: @guitar.model
    fill_in "Nut width", with: @guitar.nut_width
    fill_in "Offers", with: @guitar.offers
    fill_in "Price", with: @guitar.price
    fill_in "Wood type", with: @guitar.wood_type
    click_on "Update Guitar"

    assert_text "Guitar was successfully updated"
    click_on "Back"
  end

  test "should destroy Guitar" do
    visit guitar_url(@guitar)
    click_on "Destroy this guitar", match: :first

    assert_text "Guitar was successfully destroyed"
  end
end
