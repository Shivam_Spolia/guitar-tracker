require "test_helper"

class GuitarsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @guitar = guitars(:one)
  end

  test "should get index" do
    get guitars_url
    assert_response :success
  end

  test "should get new" do
    get new_guitar_url
    assert_response :success
  end

  test "should create guitar" do
    assert_difference("Guitar.count") do
      post guitars_url, params: { guitar: { body_type: @guitar.body_type, brand: @guitar.brand, bridge_type: @guitar.bridge_type, case_included: @guitar.case_included, comments: @guitar.comments, condition: @guitar.condition, link: @guitar.link, model: @guitar.model, nut_width: @guitar.nut_width, offers: @guitar.offers, price: @guitar.price, wood_type: @guitar.wood_type } }
    end

    assert_redirected_to guitar_url(Guitar.last)
  end

  test "should show guitar" do
    get guitar_url(@guitar)
    assert_response :success
  end

  test "should get edit" do
    get edit_guitar_url(@guitar)
    assert_response :success
  end

  test "should update guitar" do
    patch guitar_url(@guitar), params: { guitar: { body_type: @guitar.body_type, brand: @guitar.brand, bridge_type: @guitar.bridge_type, case_included: @guitar.case_included, comments: @guitar.comments, condition: @guitar.condition, link: @guitar.link, model: @guitar.model, nut_width: @guitar.nut_width, offers: @guitar.offers, price: @guitar.price, wood_type: @guitar.wood_type } }
    assert_redirected_to guitar_url(@guitar)
  end

  test "should destroy guitar" do
    assert_difference("Guitar.count", -1) do
      delete guitar_url(@guitar)
    end

    assert_redirected_to guitars_url
  end
end
