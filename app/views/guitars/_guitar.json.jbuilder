json.extract! guitar, :id, :link, :brand, :model, :body_type, :wood_type, :price, :condition, :offers, :nut_width, :bridge_type, :case_included, :comments, :created_at, :updated_at
json.url guitar_url(guitar, format: :json)
