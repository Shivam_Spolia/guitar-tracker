json.extract! inventory, :id, :brand, :model, :body_type, :wood_type, :finish, :price, :condition, :nut_width, :comments, :created_at, :updated_at
json.url inventory_url(inventory, format: :json)
