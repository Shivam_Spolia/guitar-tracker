class CreateInventories < ActiveRecord::Migration[7.1]
  def change
    create_table :inventories do |t|
      t.text :brand
      t.text :model
      t.text :body_type
      t.text :wood_type
      t.text :finish
      t.text :price
      t.text :condition
      t.text :nut_width
      t.text :comments

      t.timestamps
    end
  end
end
