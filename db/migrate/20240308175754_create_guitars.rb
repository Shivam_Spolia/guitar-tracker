class CreateGuitars < ActiveRecord::Migration[7.1]
  def change
    create_table :guitars do |t|
      t.text :link
      t.text :brand
      t.text :model
      t.text :body_type
      t.text :wood_type
      t.text :price
      t.text :condition
      t.text :offers
      t.text :nut_width
      t.text :case_included
      t.text :comments

      t.timestamps
    end
  end
end
