Rails.application.routes.draw do
  devise_for :users
  resources :inventories
  resources :guitars
  get 'home/tracker'
  root 'home#index'

  get "up" => "rails/health#show", as: :rails_health_check


end
